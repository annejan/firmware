################################################################################
 # Copyright (C) 2016 Maxim Integrated Products, Inc., All Rights Reserved.
 #
 # Permission is hereby granted, free of charge, to any person obtaining a
 # copy of this software and associated documentation files (the "Software"),
 # to deal in the Software without restriction, including without limitation
 # the rights to use, copy, modify, merge, publish, distribute, sublicense,
 # and/or sell copies of the Software, and to permit persons to whom the
 # Software is furnished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included
 # in all copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 # OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 # IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
 # OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 # ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 # OTHER DEALINGS IN THE SOFTWARE.
 #
 # Except as contained in this notice, the name of Maxim Integrated
 # Products, Inc. shall not be used except as stated in the Maxim Integrated
 # Products, Inc. Branding Policy.
 #
 # The mere transfer of this software does not imply any licenses
 # of trade secrets, proprietary technology, copyrights, patents,
 # trademarks, maskwork rights, or any other form of intellectual
 # property whatsoever. Maxim Integrated Products, Inc. retains all
 # ownership rights.
 #
 # $Date: 2018-09-28 18:22:39 +0000 (Fri, 28 Sep 2018) $ 
 # $Revision: 38193 $
 #
 ###############################################################################

# This is the name of the build output file
ifeq "$(PROJECT)" ""
PROJECT=max32665
endif

# Specify the target processor
ifeq "$(TARGET)" ""
TARGET=MAX32665
endif

# Create Target name variables
TARGET_UC:=$(shell echo $(TARGET) | tr a-z A-Z)
TARGET_LC:=$(shell echo $(TARGET) | tr A-Z a-z)

# Select 'GCC' or 'IAR' compiler
COMPILER=GCC

# Specify the board used
ifeq "$(BOARD)" ""
#BOARD=EvKit_V1
BOARD=card10
endif

# This is the path to the CMSIS root directory
ifeq "$(MAXIM_PATH)" ""
LIBS_DIR=../sdk/Libraries
else
LIBS_DIR=/$(subst \,/,$(subst :,,$(MAXIM_PATH))/Firmware/$(TARGET_UC)/Libraries)
endif
CMSIS_ROOT=$(LIBS_DIR)/CMSIS

# Source files for this test (add path to VPATH below)
SRCS  = main.c
SRCS  += pmic.c
SRCS  += bosch.c
SRCS  += bhy_support.c bhy_uc_driver.c bhy.c
SRCS  += MAX77650-Arduino-Library.c
SRCS  += bme680.h
SRCS  += bma400.c
SRCS  += LCD_Driver.c
SRCS  += GUI_Paint.c
SRCS  += DEV_Config.c
SRCS  += font24.c
SRCS  += font24CN.c
SRCS  += card10.c
SRCS  += display.c
SRCS  += leds.c


# Where to find source files for this test
VPATH = .

# Where to find header files for this test
IPATH = .

IPATH += ../lib/card10
VPATH += ../lib/card10

IPATH += ../lib/bosch/BHy1_driver_and_MCU_solution/driver/inc
IPATH += ../lib/bosch/BHy1_driver_and_MCU_solution/examples/firmware/
VPATH += ../lib/bosch/BHy1_driver_and_MCU_solution/driver/src

IPATH += ../lib/maxim/MAX77650-Arduino-Library
VPATH += ../lib/maxim/MAX77650-Arduino-Library

IPATH += ../lib/bosch/BME680_driver
VPATH += ../lib/bosch/BME680_driver

IPATH += ../lib/bosch/BMA400-API
VPATH += ../lib/bosch/BMA400-API

IPATH += ../lib/gfx
IPATH += ../lib/gfx/LCD
IPATH += ../lib/gfx/GUI_DEV
IPATH += ../lib/gfx/Fonts

VPATH += ../lib/gfx
VPATH += ../lib/gfx/LCD
VPATH += ../lib/gfx/GUI_DEV
VPATH += ../lib/gfx/Fonts


# Enable assertion checking for development
PROJ_CFLAGS+=-DMXC_ASSERT_ENABLE

# Specify the target revision to override default
# "A2" in ASCII
# TARGET_REV=0x4132

# Use this variables to specify and alternate tool path
#TOOL_DIR=/opt/gcc-arm-none-eabi-4_8-2013q4/bin

# Use these variables to add project specific tool options
#PROJ_CFLAGS+=--specs=nano.specs
#PROJ_LDFLAGS+=--specs=nano.specs

PROJ_CFLAGS+=-std=c99

# Point this variable to a startup file to override the default file
#STARTUPFILE=start.S

# Override the default optimization level using this variable
#MXC_OPTIMIZE_CFLAGS=-O1

# Point this variable to a linker file to override the default file
ifneq "$(APP)" ""
LINKERFILE=$(CMSIS_ROOT)/Device/Maxim/$(TARGET_UC)/Source/GCC/$(TARGET_LC)_app.ld
endif

################################################################################
# Include external library makefiles here

# Include the BSP
BOARD_DIR=$(LIBS_DIR)/Boards/$(BOARD)
include $(BOARD_DIR)/board.mk

# Include the peripheral driver
PERIPH_DRIVER_DIR=$(LIBS_DIR)/$(TARGET_UC)PeriphDriver
include $(PERIPH_DRIVER_DIR)/periphdriver.mk

################################################################################
# Include the rules for building for this target. All other makefiles should be
# included before this one.
include $(CMSIS_ROOT)/Device/Maxim/$(TARGET_UC)/Source/$(COMPILER)/$(TARGET_LC).mk

# The rule to clean out all the build products.
distclean: clean
	$(MAKE) -C ${PERIPH_DRIVER_DIR} clean
